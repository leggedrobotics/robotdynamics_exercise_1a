% CLASSDEF obj = IRB120Visualization3DCLASS()
%
% -> generates a visualization of the IRB120 consisting of base and 6 moving
% links
%
% obj.load(), obj.load(fig_handle)
% -> loads the patchSets in its actual configuration in the figure
%
% obj.updat(q)
% with: * q_new = [q1,q2,q3,q4,q5,q6]  generalized joint coordinates [rad]
% -> updates the structure to the new joint angles
%
% Created by Marco Hutter on 29.8.2015
% for Matlab 2013a
% -> mahutter@ethz.ch
%
%
classdef IRB120Visualization3DCLASS < handle
    
    properties
               
        % patches
        linkArr_
        
        % start configuration
        rotCenterPoint_
        rotAxis_
                
    end
    
    methods
        function obj = IRB120Visualization3DCLASS()
            figure('Name','3D visualization IRB120','Position',[100 100 800 600]);
            hold on
            set(gcf,'Color',[1 1 1])
            set(gca,'Color',[1 1 1])
            axis equal
            axis off
            axis vis3d
  
            mat.FaceColor = [210,112,16]/255;
            mat.EdgeColor = 'none';
            %mat.FaceLighting = 'phong';
            mat.AmbientStrength = 0.3;
            mat.DiffuseStrength = 0.3;
            mat.SpecularStrength = 1;
            mat.SpecularExponent = 25;
            mat.SpecularColorReflectance = 0.5;
            
            obj.linkArr_{1} = PatchCLASS('visualization/STLs/base.stl',mat);
            for i=1:6
                obj.linkArr_{i+1} = PatchCLASS(['visualization/STLs/link',num2str(i),'.stl'],mat);
            end
            
            obj.rotCenterPoint_{1}=[0,0,0];
            obj.rotAxis_{1}=[0,0,1];
            
            obj.rotCenterPoint_{2}=[0,0,290];
            obj.rotAxis_{2}=[0,1,0];
            
            obj.rotCenterPoint_{3}=[0,0,560];
            obj.rotAxis_{3}=[0,1,0];
            
            obj.rotCenterPoint_{4}=[0,0,630];
            obj.rotAxis_{4}=[1,0,0];
            
            obj.rotCenterPoint_{5}=[302,0,630];
            obj.rotAxis_{5}=[0,1,0];
            
            obj.rotCenterPoint_{6}=[0,0,630];
            obj.rotAxis_{6}=[1,0,0];

        end
        
        function [] = reset(obj)
            for i=1:length(obj.linkArr_)
                obj.linkArr_{i}.reset();
            end            
        end
        
        function [] = load(obj,varargin)
            % load()
            % load(fig_handle)
            if nargin==2
                figure(varargin{1});
            else
                figure('Name','3D visualization IBR120','Position',[100 100 800 600]);
            end
            set(gcf,'Color',[1 1 1])
            set(gca,'Color',[1 1 1])
            axis equal
            axis off
            axis vis3d
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            for i=1:length(obj.linkArr_)
                obj.linkArr_{i}.load();   
            end
            
        end
      
        function [] = setJointPositions(obj, q)
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % move all elements to the correct new position
            if length(q)~=length(obj.rotAxis_)
                error('Wrong dimension of q, it should be length 6.');
            end
            obj.reset();
            for j=length(q):-1:1
                for i=(j+1):length(obj.linkArr_)
                    rotate(obj.linkArr_{i}.p_,obj.rotAxis_{j},rad2deg(q(j)),obj.rotCenterPoint_{j})
                end
            end
            
        end
        
    end
    
end


