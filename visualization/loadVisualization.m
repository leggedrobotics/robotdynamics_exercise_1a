% Load visualization files.
load IRB120VisualizationModel.mat
abbRobot = h; clear h;
abbRobot.load();

% Set lighting.
lightangle(-120,  30);
lightangle( 120, -30);
lightangle(-120, -30);
lightangle( 120,  30);
lightangle(-120,  30);
lightangle( 120, -30);
lightangle(-120, -30);
lightangle( 120,  30);

% Set view angle.
view(24,33);
set(gca,'CameraPosition',  [2679.17 -4731.06 3838.21], ...
        'CameraTarget',    [381.33   429.964 169.419], ...
        'CameraViewAngle', 11.3725);