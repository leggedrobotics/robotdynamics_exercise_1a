function TI0 = getTransformI0()
  % Input: void
  % Output: homogeneous transformation Matrix from frame 0 to the inertial frame I. T_I0
end
